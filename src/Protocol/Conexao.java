package Protocol;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Aluno
 */
public class Conexao extends ProtocolMethods implements Runnable {

    /**
     * @return the IPsConectados
     */
    public static ArrayList<String> getIPsConectados() {
        return IPsConectados;
    }

    private volatile Socket sock;
    private volatile boolean Estado;
    private volatile String IP;
    private static ArrayList<String> IPsConectados;

    public Conexao(Socket sock) {
        try {
            this.sock = sock;
            output = new ObjectOutputStream(sock.getOutputStream());
            input = new ObjectInputStream(sock.getInputStream());
            this.IP = sock.getInetAddress().getHostAddress();
            if (IPsConectados == null) {
                IPsConectados = new ArrayList();
            }
            if (IPsConectados.indexOf(IP) == -1) {
                IPsConectados.add(IP);
            }

        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @Override
    public void run() {
        try {
            String mens;
            System.out.println("Conexão realizada por " + sock.getInetAddress().getHostAddress() + " : "
                    + sock.getPort());

            if (startConnectionServer()) {
                this.Estado = true;
                do {
                    mens = receiveMessage();
                    if (mens.equals("#@Message#")) {
                        mens = (String) receiveTransmission();
                        System.out.println("mensagem recebida de " + sock.getInetAddress()
                                .getHostAddress() + " : " + mens);
                    } else if (mens.equals("#@ListConnection#")) {
                        send("#@accept#");
                        sendTransmission(getIPsConectados());
                    }
                } while (!mens.isEmpty() && !closeConnectionServer(mens));

                removeIP();
                System.out.println("Desconectado " + sock.getInetAddress().getHostAddress() + " : "
                        + sock.getPort());
                output.close();
                input.close();
                sock.close();
                this.Estado = false;
            }
        } catch (IOException e) {
            System.out.println("2" + e);
        }
    }

    protected void removeIP() {
        /////Remover da lista de ips conectados
        if (getIPsConectados() != null && getIPsConectados().size() > 0) {
            for (int i = 0; i < getIPsConectados().size(); i++) {
                if (sock.getInetAddress().getHostAddress().equals(getIPsConectados().get(i))) {
                    getIPsConectados().remove(i);
                }
            }
        }
    }

    /**
     * @return the Estado
     */
    public boolean isEstado() {
        return Estado;
    }

    /**
     * @return the IP
     */
    public String getIP() {
        return IP;
    }

    /**
     * @param aIPsConectados the IPsConectados to set
     */
    public static void setIPsConectados(ArrayList<String> aIPsConectados) {
        IPsConectados = aIPsConectados;
    }

    public static void addIps(ArrayList<String> aIPsConectados) {
        if (IPsConectados == null) {
            IPsConectados = new ArrayList();
        }
        for (String aIPsConectado : aIPsConectados) {
            if (IPsConectados.indexOf(aIPsConectado) == -1) {
                IPsConectados.add(aIPsConectado);
            }
        }
    }
}
