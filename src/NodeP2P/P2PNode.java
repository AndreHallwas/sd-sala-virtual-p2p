/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NodeP2P;

import Protocol.Cliente;
import Protocol.Conexao;
import Protocol.Servidor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Aluno
 */
public class P2PNode {

    private Servidor servidor;
    private ArrayList<Cliente> cliente;
    private Thread Terminal;

    public P2PNode() {
        servidor = new Servidor(8001, 50) {
        };

        servidor.Iniciar();
        cliente = new ArrayList();

        Inicializar();

        verificaConexoes();
    }

    protected void verificaConexoes() {
        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                if (Conexao.getIPsConectados() != null && Conexao.getIPsConectados() != null) {
                    ArrayList<String> Atuais = new ArrayList();
                    for (Cliente oCliente : cliente) {
                        Atuais.add(oCliente.getIP());
                    }
                    ArrayList<String> Novos = Conexao.getIPsConectados();
                    ArrayList<String> Adicionados = new ArrayList();
                    ArrayList<String> Removidos = new ArrayList();
                    ArrayList<String> NaoModificados = new ArrayList();
                    Removidos.addAll(Atuais);
                    for (String oNovo : Novos) {
                        int i = 0;
                        while (i < Atuais.size() && !Atuais.get(i).equalsIgnoreCase(oNovo)) {
                            i++;
                        }
                        if (i < Atuais.size()) {
                            NaoModificados.add(Atuais.get(i));
                            Removidos.remove(Atuais.get(i));
                        } else {
                            Adicionados.add(oNovo);
                        }
                    }
                    for (String Removido : Removidos) {
                        int i = 0;
                        while (i < cliente.size() && !cliente.get(i).getIP().equalsIgnoreCase(Removido)) {
                            i++;
                        }
                        if (i < cliente.size()) {
                            cliente.get(i).FinalizarConexao();
                            cliente.remove(i);
                        }
                    }
                    for (String Adicionado : Adicionados) {
                        Cliente cli = new Cliente(Adicionado, 8001);
                        cliente.add(cli);
                        cli.inicializar();
                    }
                }
            }
        }, 200, 8000);
    }

    public void Inicializar() {

        Terminal = new Thread(new Runnable() {
            @Override
            public void run() {
                String Message;
                Scanner in = new Scanner(System.in);
                Message = in.next();
                while (!Message.equalsIgnoreCase("exit")) {
                    try {
                        if (Message.equalsIgnoreCase("List")) {
                            if (cliente != null
                                    && cliente.size() > 0) {
                                for (Cliente iPsConectado : cliente) {
                                    System.out.println(iPsConectado.getIP());
                                }
                            } else {
                                System.out.println("0 Connections;");
                            }
                        } else if (Message.equalsIgnoreCase("info")) {
                            System.out.println("IP: " + servidor.getServer().getInetAddress().getHostName());
                            System.out.println("Porta: " + servidor.getServer().getLocalPort());
                            System.out.println("Ligado: " + !servidor.getServer().isClosed());
                            System.out.println("Timeout: " + servidor.getServer().getSoTimeout());
                            System.out.println("Conexões Disponiveis: " + servidor.getServer().getReceiveBufferSize());
                            System.out.println("Details: " + servidor.getServer().toString());
                            System.out.println("Working !!!!!");
                        } else if (Message.equalsIgnoreCase("Message")) {
                            String IP = in.next();
                            int i;
                            for (i = 0; i < cliente.size() && !cliente.get(i).getIP().equalsIgnoreCase(IP); i++);
                            if (i < cliente.size()) {
                                Message = in.next();
                                cliente.get(i).Transmitir(Message);
                            }
                        } else {
                            System.out.println("Não Reconhecido: " + Message);
                        }

                        Message = in.next();
                    } catch (IOException ex) {
                        System.out.println(ex);
                    }
                }
            }
        });
        Terminal.start();
    }

    public void conecta(String IP) {
        Cliente cli;
        cliente.add(cli = new Cliente(IP, 8001));
        cli.inicializar();
    }

    public static void main(String[] IP) {
        P2PNode nc = new P2PNode();
        nc.conecta("127.0.0.1");
    }

    /**
     * @return the servidor
     */
    public Servidor getServidor() {
        return servidor;
    }

    /**
     * @return the clientes
     */
    public ArrayList<Cliente> getCliente() {
        return cliente;
    }

}
