/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Protocol;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public abstract class ProtocolMethods {
    /*
    *   #@Connect# ///Conexão
        #@Disconnect# ///Desconectar
        #@Message# ///Transmissao de Mensagem
        #@accept# ///Aceita uma transmissao, conexao, ou mensagem
        #@decline# ///Rejeita uma transmissao, conexao, ou mensagem
        #@Received# //recebeu a mensagem
        #@notReceived# //não recebeu a mensagem
        #@transmitting# //transmitindo
        #@EndTransmission# //transmissao terminada
        #@RemoveConnection# //Remove uma conexão
        #@ListConnection# //Exibe
        //8001 = Porta Envia
        //8002 = Porta Recebe
    */
    protected volatile ObjectOutputStream output;
    protected volatile ObjectInputStream input;

    protected boolean startConnectionServer() {
        String mens = receiveMessage();
        if (mens.equals("#@Connect#")) {
            send("#@accept#");
            return true;
        }
        return false;
    }

    protected boolean closeConnectionServer(String mens) {
        if (mens.equals("#@Disconnect#")) {
            send("#@accept#");
            return true;
        }
        return false;
    }

    protected boolean startConnectionClient() {
        boolean Result = false;
        try {
            output.writeObject("#@Connect#");
            String mensagem;
            do {
                mensagem = receiveMessage();
            } while (!mensagem.equals("#@accept#") && !mensagem.equals("#@decline#"));
            if (mensagem.equals("#@accept#")) {
                Result = true;
            }
        } catch (Exception ex) {
            System.out.println("Erro: " + ex);
        }
        return Result;
    }

    protected boolean closeConnectionClient() {
        boolean Result = false;
        String mens;
        send("#@Disconnect#");
        mens = receiveMessage();
        if (mens.equals("#@accept#")) {
            Result = true;
        }
        return Result;
    }

    protected void send(Object Mensagem) {
        try {
            output.flush();
            output.writeObject(Mensagem);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    protected String receiveMessage() {
        String Mensagem;
        try {
            Mensagem = (String) input.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex);
            Mensagem = "";
        }
        return Mensagem;
    }

    protected Object receiveTransmission() {
        ArrayList<Object> Objects = new ArrayList();
        Object mens;
        send("#@accept#");
        mens = receiveMessage();
        if (mens.equals("#@transmitting#")) {
            mens = receiveMessage();
            Object auxTransmission = "";
            do {
                Objects.add(auxTransmission = mens);
                mens = receiveMessage();
            } while (!mens.equals("#@EndTransmission#"));
            send("#@Received#");
            mens = auxTransmission;
        }
        return Objects.size() > 1 ? Objects : mens;
    }

    protected void sendTransmission(Object Message) {
        String mens;
        do {
            send("#@transmitting#");
            send(Message);
            send("#@EndTransmission#");
            mens = receiveMessage();
        } while (!mens.equals("#@Received#")
                && !mens.equals("#@Disconnect#"));
    }
    
    protected void sendMessageTransmission(String Message){
        String mens;
        send("#@Message#");
        mens = receiveMessage();
        if (mens.equals("#@accept#")) {
            send("#@transmitting#");
            send(Message);
            send("#@EndTransmission#");
            mens = receiveMessage();
            if (!mens.equals("#@Received#")) {
                System.out.println("Mensagem não enviada, pois o servidor não confirmou recebimento");
            } else {
                System.out.println("Mensagem foi Enviada");
            }
        } else {
            System.out.println("O envio não foi aceito");
        }
    }

}
