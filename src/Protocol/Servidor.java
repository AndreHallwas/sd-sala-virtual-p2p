package Protocol;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Aluno
 */
public abstract class Servidor {

    private volatile ServerSocket server;
    protected volatile Socket sock;
    protected volatile String mens;
    protected volatile Thread Serv;
    protected volatile Thread Terminal;
    protected volatile Integer Porta;
    protected volatile Integer QtdConexoes;
    protected volatile ArrayList<Conexao> Conexoes = new ArrayList();
    private boolean isOpen;

    public Servidor(Integer Porta, Integer Conexoes) {
        this.Porta = Porta;
        this.QtdConexoes = Conexoes;
    }

    public void RecebeConexoes() {
        try {
            server = new ServerSocket(Porta, QtdConexoes); //esperando conexão na porta 5000
            System.out.println("Esperando conexão....");
            isOpen = true;
            while (isOpen) {
                sock = getServer().accept(); // a espera de conexões (listen)
                Conexao con = new Conexao(sock);
                Conexoes.add(con);
                new Thread(con).start();
            }
        } catch (Exception e) {
            System.out.println("Erro: " + e);
        }
    }
    
    public void Iniciar(){
        Serv = new Thread(new Runnable() {
            @Override
            public void run() {
                RecebeConexoes();
            }
        });
        Serv.start();
    }

    public void Inicializar() {

        Iniciar();
        Terminal = new Thread(new Runnable() {
            @Override
            public void run() {
                String Message;
                Scanner in = new Scanner(System.in);
                Message = in.next();
                while (!Message.equalsIgnoreCase("exit")) {
                    try {
                        if (Message.equalsIgnoreCase("List")) {
                            if (Conexao.getIPsConectados() != null
                                    && Conexao.getIPsConectados().size() > 0) {
                                for (Object iPsConectado : Conexao.getIPsConectados()) {
                                    System.out.println(iPsConectado);
                                }
                            } else {
                                System.out.println("0 Connections;");
                            }
                        } else if (Message.equalsIgnoreCase("info")) {
                            System.out.println("IP: " + getServer().getInetAddress().getHostName());
                            System.out.println("Porta: " + getServer().getLocalPort());
                            System.out.println("Ligado: " + !server.isClosed());
                            System.out.println("Timeout: " + getServer().getSoTimeout());
                            System.out.println("Conexões Disponiveis: " + getServer().getReceiveBufferSize());
                            System.out.println("Details: " + getServer().toString());
                            System.out.println("Working !!!!!");
                        } else {
                            System.out.println("Não Reconhecido: " + Message);
                        }

                        Message = in.next();
                    } catch (IOException ex) {
                        System.out.println(ex);
                    }
                }
            }
        });
        Terminal.start();
    }

    public static void main(String[] args) {
        Servidor s = new Servidor(8001, 50) {
        };
        s.Inicializar();
    }

    /**
     * @return the server
     */
    public ServerSocket getServer() {
        return server;
    }
}
