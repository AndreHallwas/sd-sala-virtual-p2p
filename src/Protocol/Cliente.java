package Protocol;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import javax.swing.text.StyleConstants;

/**
 *
 * @author Aluno
 */
public class Cliente extends ProtocolMethods {

    private volatile Socket Client;
    private volatile ObjectOutputStream output;
    private volatile ObjectInputStream input;
    protected volatile String IP;
    private volatile Integer Porta;

    public Cliente(String IP, Integer Porta) {
        this.IP = IP;
        this.Porta = Porta;
    }

    public void Transmitir(String mensg) {
        try {
            if (!Client.isClosed() && Client != null) {

                String mensagem;
                output.flush();
                output.writeObject("#@Message#");
                mensagem = (String) input.readObject();
                if (mensagem.equals("#@accept#")) {
                    output.flush();
                    output.writeObject("#@transmitting#");
                    output.flush();
                    output.writeObject(mensg);
                    output.flush();
                    output.writeObject("#@EndTransmission#");

                    mensagem = (String) input.readObject();
                    if (!mensagem.equals("#@Received#")) {
                        System.out.println("Mensagem não enviada, pois o servidor não confirmou recebimento");
                    } else {
                        System.out.println("Mensagem foi Enviada");
                    }

                } else {
                    System.out.println("O envio não foi aceito");
                }
            }
        } catch (Exception ex) {
            System.out.println("Erro: " + ex);
        }
    }

    public void IniciarConexao() {
        try {
            if (Client == null || Client.isClosed()) {
                Client = new Socket(getIP(), Porta);
                output = new ObjectOutputStream(Client.getOutputStream());
                input = new ObjectInputStream(Client.getInputStream());
                output.writeObject("#@Connect#");
                String mensagem;
                do {
                    mensagem = (String) input.readObject();
                } while (!mensagem.equals("#@accept#") && !mensagem.equals("#@decline#"));

                if (mensagem.equals("#@decline#")) {
                    output.flush();
                    output.close();
                    input.close();
                    Client.close();
                } else if (mensagem.equals("#@accept#")) {
                    output.flush();
                    output.writeObject("#@ListConnection#");
                    mensagem = (String) input.readObject();
                    if (mensagem.equals("#@accept#")) {
                        mensagem = (String) input.readObject();
                        if (mensagem.equals("#@transmitting#")) {
                            /////Conexao.setIPsConectados(new ArrayList((Collection) input.readObject()));
                            Conexao.addIps(new ArrayList((Collection) input.readObject()));
                            ArrayList aux = new ArrayList();
                            aux.add(getIP());
                            Conexao.addIps(aux);
                            mensagem = (String) input.readObject();
                            if (mensagem.equals("#@EndTransmission#")) {
                                output.flush();
                                output.writeObject("#@Received#");
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("Erro: " + ex);
        }
    }

    public void FinalizarConexao() {
        try {
            if (!Client.isClosed()) {
                output.writeObject("#@Disconnect#");
                if (((String) input.readObject()).equals("#@accept#")) {
                    output.flush();
                    output.close();
                    input.close();
                    Client.close();
                }
            }
        } catch (Exception ex) {
            System.out.println("Erro: " + ex);
        }
    }

    public void inicializar() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                IniciarConexao();
            }
        }).start();

    }

    public static void main(String[] args) {
        for (int i = 0; i < 5500; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Random r = new Random();
                    try {
                        Thread.sleep(r.nextInt(50000));
                    } catch (InterruptedException ex) {
                        System.out.println("Erro na thread");
                    }
                    Cliente c = new Cliente("127.0.0.1", 8001);
                    c.IniciarConexao();
                    c.Transmitir("teste");
                    c.FinalizarConexao();
                }
            }).start();
        }
    }

    /**
     * @return the IP
     */
    public String getIP() {
        return IP;
    }
}
